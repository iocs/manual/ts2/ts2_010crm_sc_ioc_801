#
# Module: essioc
#
require essioc

#
# Module: cabtr
#
require cabtr


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${cabtr_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: TS2-010CRM:Cryo-TC-001
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_monitor.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TC-001, IPADDR = ts2-cabtr-01.tn.esss.lu.se, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-069
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-069, CONTROLLER = TS2-010CRM:Cryo-TC-001, CHANNEL = 1, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-023
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-023, CONTROLLER = TS2-010CRM:Cryo-TC-001, CHANNEL = 2, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-091
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-091, CONTROLLER = TS2-010CRM:Cryo-TC-001, CHANNEL = 3, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-092
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-092, CONTROLLER = TS2-010CRM:Cryo-TC-001, CHANNEL = 4, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-093
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-093, CONTROLLER = TS2-010CRM:Cryo-TC-001, CHANNEL = 5, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-094
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-094, CONTROLLER = TS2-010CRM:Cryo-TC-001, CHANNEL = 6, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-030
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-030, CONTROLLER = TS2-010CRM:Cryo-TC-001, CHANNEL = 7, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-049
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-049, CONTROLLER = TS2-010CRM:Cryo-TC-001, CHANNEL = 8, POLL = 1000")
